# coding: utf8

import requests
from datetime import datetime

#tab_name = tab_to_test(reset=True)

# Aggiungere colonne alla tabella di test
db.define_table('test',
    Field('inserted', 'datetime', compute=lambda x: datetime.now),
    Field('name'),
    Field('lastname'),
    Field('age', 'integer'),
    Field('birth', 'date'),
    Field('death', 'datetime'),
    Field('obj', 'json'),
    Field('description0', 'text'),
    Field('description1', 'text'),
    Field('description2', 'text'),
    Field('description3', 'text'),
    Field('description4', 'text'),
    Field('male', 'boolean'),
    Field('alarm0', 'time'),
    Field('alarm1', 'time'),
    Field('alarm2', 'time'),
    Field('alarm3', 'time'),
    Field('pressure0', 'double'),
    Field('pressure1', 'double'),
    Field('pressure2', 'double'),
    Field('pressure3', 'double'),
    Field('pressure4', 'double'),
    Field('temperature0', 'float'),
    Field('temperature1', 'float'),
    Field('temperature2', 'float'),
    Field('temperature3', 'float'),
    Field('temperature4', 'float'),
    Field('something0', 'integer'),
    Field('something1', 'integer'),
    Field('something2', 'integer'),
    Field('something3', 'integer'),
    Field('something4', 'integer'),
    #Field('measure', 'decimal') # non usare decimal
)

def populate_table():

    n = 2 # >= 2

    ids = []
    errors = []

    def foo(res=None):
        print 'Well done!'
        reactor.stop()

    def bar(err=None):
        errors.append(err)
        print 'Errors!'
        reactor.stop()

    populators = []
    for i in range(1, n):
        name = 'Pop. n. %s' % n
        populators.append(Populator(db.test, name=name, freq=1).start())

    dl = defer.DeferredList(populators)
    dl.addCallbacks(foo, bar)
