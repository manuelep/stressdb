# coding: utf8

from gluon.contrib.populate import populate

from twisted.internet import defer, reactor

#if not reactor.running:
    #reactor.run()

class Trigger(object):

    def __init__(self, table, fifo_limit):
        self.db = table._db
        self.table = table
        self.fifo_limit = fifo_limit

    def clean(self, f):
        if self.db(self.table.id>0).count() >= self.fifo_limit:
            Min = self.table.id.min()
            minimum = self.db(self.table.id>0).select(Min).first()[Min]
            del self.table[minimum]


class Populator(object):

    def __init__(self, table, name='', total_recs=100, partial_recs=1, freq=1, FIFO=.7):

        if FIFO > 0:
            table._before_insert.append(Trigger(table, fifo_limit=total_recs*FIFO).clean)

        self.name = name
        self.table = table
        self.counter = total_recs
        self.nins = partial_recs
        self.freq = freq
        self.defer = defer.Deferred()

    def start(self):
        self.populate()
        return self.defer

    def populate(self):
        if self.counter < 1:
            self.defer.callback(True)
        elif self.counter > self.nins:
            reactor.callLater(1/self.freq, self.populate)
            self.counter += -self.nins
            if self.counter == 40:
                raise Exception('Errore voluto!')
            else:
                populate(self.table, self.nins, compute=True)
        else:
            reactor.callLater(1/self.freq, self.populate)
            self.counter = 0
            populate(self.table, self.nins-self.counter, compute=True)

