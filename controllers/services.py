# coding: utf8
# prova qualcosa come

from gluon.contrib.populate import populate

#def index(): return dict(message="hello from services.py")

@service.json
def insert():

    tab_name = request.args(0)

    out = dict()

    try:
        id = db[tab_name].insert(**request.vars)
    except Exception as error:
        out['error'] = '%s' % type(error)
        out['message'] = '%s' % error
    else:
        out['id'] = id

    return out

@service.json
def filltab():

    tab_name = request.args(0)
    records = request.args(1) or 1

    out = dict()

    try:
        populate(db[tab_name], records)
    except Exception as error:
        out['error'] = '%s' % type(error)
        out['message'] = '%s' % error
    else:
        db.commit()
        out['id'] = db._adapter.lastrowid(db[tab_name])

    return out

def scheduler_tasks():
    grid = SQLFORM.smartgrid(skdb.scheduler_task,linked_tables=['scheduler_run'])
    return dict(grid=grid)

def test():
    return dict(errs= session.test_errors)
